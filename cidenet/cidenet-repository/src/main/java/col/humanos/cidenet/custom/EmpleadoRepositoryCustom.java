package col.humanos.cidenet.custom;

import col.humanos.cidenet.model.Empleado;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EmpleadoRepositoryCustom {

  /**
   * Metodo que permite realizar la busqueda avanzada  empleados apartir de los filtrados ingresados
   *
   * @param busqueda filtros realizados
   * @param pageable paginador
   * @return Empleado paginada
   */
  Page<Empleado> busquedaAvanzada(Empleado busqueda, Pageable pageable);
}
