package col.humanos.cidenet.impl;

import col.humanos.cidenet.custom.EmpleadoRepositoryCustom;
import col.humanos.cidenet.model.Empleado;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.query.QueryUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class EmpleadoRepositoryImpl implements EmpleadoRepositoryCustom {

  private static final Logger LOGGER = LoggerFactory.getLogger(EmpleadoRepositoryImpl.class);

  @PersistenceContext
  private EntityManager em;

  @Override
  public Page<Empleado> busquedaAvanzada(Empleado busqueda, Pageable pageable) {

    try {
      LOGGER.info("Entramos en PruebaServicioInstalacion.busquedaAvanzada");

      List<Predicate> predicates = new ArrayList<>();
      CriteriaBuilder cbPrueba = em.getCriteriaBuilder();
      CriteriaQuery<Empleado> criteria = cbPrueba.createQuery(Empleado.class);
      Root<Empleado> rootPrueba = criteria.from(Empleado.class);

      criteria.where(cbPrueba.and(predicates.toArray( new Predicate[predicates.size()])));

      criteria.orderBy(QueryUtils.toOrders(pageable.getSort(), rootPrueba, cbPrueba));

      List<Empleado> result = em.createQuery(criteria).setFirstResult((int) pageable.getOffset())
                                                       .setMaxResults(pageable.getPageSize()).getResultList();

      CriteriaQuery<Long> countQuery = cbPrueba.createQuery(Long.class);

      countQuery.where(cbPrueba.and(predicates.toArray(new Predicate[predicates.size()])));

      Root<Empleado> pruebaInstalacionRootCount = countQuery.from(Empleado.class);

      countQuery.select(cbPrueba.count(pruebaInstalacionRootCount))
        .where(cbPrueba.and(predicates.toArray(new Predicate[predicates.size()])));

      Long count = em.createQuery(countQuery).getSingleResult();
      return new PageImpl<>(result, pageable, count);

    } catch (RuntimeException e) {
      LOGGER.error("Error en Empleado.busquedaAvanzada {}", e.getMessage());
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error en Empleado.busquedaAvanzada {}", e);
      throw e;
    } finally {
      LOGGER.info("Salimos de Empleado.busquedaAvanzada");
    }

  }
}
