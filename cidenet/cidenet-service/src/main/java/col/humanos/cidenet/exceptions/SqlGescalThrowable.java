package col.humanos.cidenet.exceptions;

public class SqlGescalThrowable extends Throwable {

  /** Variable de serializacion */
  private static final long serialVersionUID = 7667868930899609326L;

  public SqlGescalThrowable(String mensaje) {
    super(mensaje);
  }
}
